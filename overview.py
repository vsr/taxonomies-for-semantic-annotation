"""prints an overview over the current coverage of collection terms and contribution by found taxonomies"""

import argparse
import json
import os

from evaluator import prep, print_summary


def argparser():
    """
    Builds the argparse parser and reads the command line parameters
    """
    parser = argparse.ArgumentParser(description="shows an overview across the already converted taxonomies")
    parser.add_argument('-e', dest='collection_terms_filename', default='input/categorized_terms.yaml',
                        help="file containing collection terms in categories")
    parser.add_argument('-i', dest='input_dir', default='output',
                        help="directory containing the taxonomies & term_collection_coverage. "
                             "Defaults to 'output' (as used by the category_coverage.py script)")
    return parser.parse_args()


def taxonomy_details(input_dir, categories):
    """
    calculates details from term_collection_coverage.json files
    """
    total_coverage = {}
    taxonomy_contribution = {}
    taxonomy_terms = {}
    for directory in os.listdir(input_dir):
        if not os.path.isdir(os.path.join(input_dir, directory)):
            continue
        if not os.path.exists(os.path.join(input_dir, directory, "term_collection_coverage.json")):
            print(f"missing term_collection_coverage.json in directory {directory}")
            continue
        with open(os.path.join(input_dir, directory, "term_collection_coverage.json")) as in_file:
            voc_info = json.load(in_file)
        match_count = 0
        matched_terms = []
        for category in categories:
            match_count += len(voc_info[category])
            for term in voc_info[category]:
                matched_terms.append(term)
            for collection_term in voc_info[category]:
                if collection_term not in total_coverage:
                    total_coverage[collection_term] = [voc_info[category][collection_term][0]]
                else:
                    total_coverage[collection_term].append(voc_info[category][collection_term][0])
        taxonomy_contribution[voc_info["_voc_id"]] = match_count
        taxonomy_terms[voc_info["_voc_id"]] = matched_terms
    return total_coverage, taxonomy_contribution, taxonomy_terms


def find_unique_matches(taxonomy_terms):
    """
    finds the number of unique terms for each taxonomy (that is terms that only appear in this
    exact taxonomy)
    """
    unique_matches = {}
    for taxonomy in taxonomy_terms:
        unique_matches[taxonomy] = 0
        for term in taxonomy_terms[taxonomy]:
            is_unique = True
            for taxonomy2 in taxonomy_terms:
                if taxonomy2 == taxonomy:
                    continue
                for term2 in taxonomy_terms[taxonomy2]:
                    if term == term2:
                        is_unique = False
                        break
                if not is_unique:
                    break
            if is_unique:
                unique_matches[taxonomy] += 1
    return unique_matches


def taxonomy_table_widths(taxonomy_contribution):
    """calculates some table widths for nicer display"""
    max_tax_name = 0
    max_tax_matches = 0
    for taxonomy, matches in taxonomy_contribution.items():
        if max_tax_name < len(taxonomy):
            max_tax_name = len(taxonomy)
        if max_tax_matches < matches:
            max_tax_matches = matches
    return max_tax_name, max_tax_matches


def coverage_details(categories, collection_terms, total_coverage):
    """calculates coverage details (e.g. percentages)"""
    counts = {}
    totals = {}
    total_coverage["_matches"] = 0
    final_collection_terms = []
    for category in categories:
        count = 0
        totals[category] = len(collection_terms[category])
        for collection_term in collection_terms[category]:
            final_collection_terms.append(collection_term["original"])
            if collection_term["original"] in total_coverage:
                count += 1
        counts[category] = count
        total_coverage["_matches"] += count
    return counts, totals, final_collection_terms


def main():
    """for commandline usage, use --help for additional info."""

    args = argparser()
    if not os.listdir(args.input_dir):
        print(f"The directory '{args.input_dir}/' does not contain any taxonomies.\n"
              f"Please ensure the presence of taxonomies in the directory and retry.")
        return

    categories, collection_terms, _ = prep(args.collection_terms_filename, False)

    print("taxonomies\n"
          "==========")

    total_coverage, \
    taxonomy_contribution, \
    taxonomy_terms = taxonomy_details(args.input_dir, categories)
    unique_matches = find_unique_matches(taxonomy_terms)

    print("matches per taxonomy\n"
          "--------------------")
    max_tax_name, max_tax_matches = taxonomy_table_widths(taxonomy_contribution)
    for taxonomy, matches in sorted(taxonomy_contribution.items(), key=lambda x: x[1]):
        print(f"{taxonomy.ljust(max_tax_name)} {str(matches).rjust(len(str(max_tax_matches)))}\n")

    print("unique matches per taxonomy\n"
          "---------------------------")
    for taxonomy, matches in sorted(unique_matches.items(), key=lambda x: x[1]):
        print(f"{taxonomy.ljust(max_tax_name)} {str(matches).rjust(len(str(max_tax_matches)))}\n")
    print("term matches\n"
          "============\n")
    counts, \
    totals, \
    final_collection_terms = coverage_details(categories, collection_terms, total_coverage)

    print("\nunmatched terms\n"
          "---------")
    for category in categories:
        print(category)
        for collection_term in collection_terms[category]:
            if collection_term["original"] not in total_coverage:
                print("\t" + collection_term["original"])

    final_summary_data = {"counts": counts, "totals": totals}
    print_summary(final_collection_terms, categories, total_coverage, final_summary_data, True)


if __name__ == "__main__":
    main()
