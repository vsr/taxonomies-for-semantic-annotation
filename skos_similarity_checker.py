"""Searches two or more skos taxonomies for possible matching concepts"""

import argparse
import itertools
import json
import multiprocessing as mp
import os

import nltk
from nltk.corpus import wordnet as wn
from tqdm import tqdm

from util import load_graph, stem, my_tokenize, auto_label_similarity, SKOS


RESULT_FILE_NAME = "output_link_results/link_results_unspecified.json"
TERMS_2 = []


def jsonify(term):
    """
    jsonifies a term, so that only the relevant information (concept-URI and skos file) gets stored
    to file
    """
    return {"concept": term["concept"], "skos_file": term["skos_file"]}


def child_initialize(_terms_2):
    """initializer for multiprocessing"""
    global TERMS_2
    TERMS_2 = _terms_2
    next(wn.words())


def my_error_callback(error):
    """error callback for multiprocessing"""
    print("error in a process:")
    print(error)


def compare_taxonomies(terms_1, terms_2, num_of_processes):
    """
    compares two taxonomies by splitting the workload using multiprocessing (kinda wonky)
    In theory this should work, if a different comparison that didn't require wordnet would be used.
    """
    print(f"comparing taxonomies with {len(terms_1)} and {len(terms_2)} terms")
    terms_per_process = len(terms_1) // num_of_processes
    results = []
    pool = mp.Pool(num_of_processes, initializer=child_initialize, initargs=(terms_2,))
    for i in range(num_of_processes):
        if i == num_of_processes-1:
            partial_terms = terms_1[terms_per_process * i:]
        else:
            partial_terms = terms_1[terms_per_process * i: terms_per_process * (i+1)]
        results.append(pool.apply_async(compare_terms, (partial_terms, i),
                                        error_callback=my_error_callback))
    pool.close()
    pool.join()
    final_results = []
    for result in results:
        for match in result.get():
            final_results.append(match)
    with open(RESULT_FILE_NAME, "w+") as result_file:
        json.dump(final_results, result_file)


def compare_terms(terms_1, p_num, _terms_2=None, use_tqdm=False, compare=auto_label_similarity):
    """
    Compares all terms of two term lists with each other. Matches are returned and also written to
    the result file as soon as they are found. If multiprocessing is used, only the process number 0
    writes the intermediate results to a file, the other processes do not.
    """
    if _terms_2 is not None:
        global TERMS_2
        TERMS_2 = _terms_2
    potential_matches = []
    my_iter = itertools.product(terms_1, TERMS_2)
    if use_tqdm:
        my_iter = tqdm(my_iter, total=(len(terms_1) * len(TERMS_2)))
    for term_1, term_2 in my_iter:
        if compare(term_1, term_2):
            if (term_1["concept"], term_2["concept"]) in potential_matches:
                continue
            potential_matches.append((jsonify(term_1), jsonify(term_2)))
            if p_num == 0:
                with open(RESULT_FILE_NAME, "w+") as result_file:
                    json.dump(potential_matches, result_file)
    return potential_matches


def argparser():
    """
    Builds the argparse parser and reads the command line parameters
    """
    parser = argparse.ArgumentParser(description="searches for skos:exactMatch candidates and "
                                     "after user verification adds them to the skos files")
    parser.add_argument(metavar="skos_file", dest="main_file", type=str,
                        help="Main SKOS file to add matches to.")
    parser.add_argument(metavar="skos_file", dest="skos_files", type=str, nargs="+",
                        help="SKOS files to compare terms to. If --all is used, these will also be "
                             "compared with each other.")
    parser.add_argument('-o', dest='output_dir', default='output_link_results/',
                        help="output directory for storing match information")
    parser.add_argument("--all", dest="compare_all", action="store_true",
                        help="If this is set, then all pairs of SKOS files will get compared. "
                             "Otherwise, only the first will be compared to the others, which are "
                             "assumed to have already been compared with each other.")
    # Number of processes to use. Defaults to 1. Broken, don't use!
    # Doesn't work because of wordnet/nltk
    parser.add_argument("-p", dest="num_of_processes", type=int, default=1, help=argparse.SUPPRESS)
    return parser.parse_args()


def search_labels(skos_graphs):
    """
    searches for all labels in the given skos graphs, that are in english.
    """
    porter_stemmer = nltk.stem.PorterStemmer()
    for graph, skos_file, terms in skos_graphs:
        for concept, label in itertools.chain(graph.subject_objects(SKOS.prefLabel),
                                              graph.subject_objects(SKOS.altLabel)):
            if hasattr(label, "language") and isinstance(label.language, str) and \
               not label.language.startswith("en"):
                continue
            terms.append({"stem": stem(label, porter_stemmer),
                          "full tokens": my_tokenize(label),
                          "concept": concept,
                          "label": label,
                          "skos_file": skos_file})


def main():
    """for commandline usage, use --help for additional info."""

    args = argparser()

    print("parsing skos files")
    skos_graphs = []
    for skos_file in [args.main_file] + args.skos_files:
        skos_graphs.append((load_graph(skos_file), skos_file, []))

    print("prepare")

    search_labels(skos_graphs)

    print("comparing")
    my_iter = [(skos_graphs[0], other_graph) for other_graph in skos_graphs[1:]]
    if args.compare_all:
        my_iter = itertools.combinations(skos_graphs, 2)
    for (_, skos_file_1, terms_1), (_, skos_file_2, terms_2) in my_iter:
        print(f"comparing {os.path.basename(skos_file_1)} with {os.path.basename(skos_file_2)}")
        global RESULT_FILE_NAME
        tax_a = os.path.basename(skos_file_1).replace('.ttl', '')
        tax_b = os.path.basename(skos_file_2).replace('.ttl', '')
        if not os.path.exists(args.output_dir):
            os.mkdir(args.output_dir)
        RESULT_FILE_NAME = f"{args.output_dir}/link_results_{tax_a}_{tax_b}.json"
        if args.num_of_processes > 1:
            compare_taxonomies(terms_1, terms_2, args.num_of_processes)
        else:
            final_results = compare_terms(terms_1, 0, terms_2, True)
            with open(RESULT_FILE_NAME, "w+") as result_file:
                json.dump(final_results, result_file)


if __name__ == "__main__":
    main()
