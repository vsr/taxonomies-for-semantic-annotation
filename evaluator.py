"""utility things for evaluating taxonomies against the collection terms"""

import itertools
import yaml
import rdflib
import nltk
from tqdm import tqdm
from util import stem, my_tokenize, covered

THRESHOLD = 0.9


def extract_eval_terms(graph):
    """finds labels in the given graph"""
    terms = []
    for _, term in itertools.chain(graph.subject_objects(rdflib.namespace.SKOS.prefLabel),
                                   graph.subject_objects(rdflib.namespace.SKOS.altLabel)):
        term = str(term)
        if "@" in term:
            term = term.split("@")[0]
        terms.append(term)
    return terms


def prep(collection_terms_filename, verbose=False):
    """reads categories and collection terms and prepares the collection terms for comparisons"""

    if verbose:
        print("reading collection terms")

    orig_collection_terms = {}

    categories = []
    with open(collection_terms_filename) as in_yaml:
        collection_terms = yaml.load(in_yaml.read(), Loader=yaml.FullLoader)
        for category in collection_terms:
            categories.append(category)
            if category not in orig_collection_terms:
                orig_collection_terms[category] = []
            for term in collection_terms[category]:
                orig_collection_terms[category].append(term)

    if verbose:
        print("stemming")

    porterstemmer = nltk.stem.PorterStemmer()

    collection_terms = {}
    for category in categories:
        collection_terms[category] = []
        for term in orig_collection_terms[category]:
            collection_terms[category].append({"original": term, "stem": stem(term, porterstemmer)})

    if verbose:
        print("tokenizing full terms")

    for category in categories:
        for term in collection_terms[category]:
            term["full tokens"] = my_tokenize(term["original"])

    if verbose:
        print("tokenizing stems")

    for category in categories:
        for term in collection_terms[category]:
            term["stem tokens"] = my_tokenize(term["stem"])

    term_lookup = {}
    for category in categories:
        for collection_term in collection_terms[category]:
            term_lookup[collection_term["original"]] = collection_term

    return categories, collection_terms, term_lookup


def prep_taxonomy(voc, verbose=False):
    """prepares the terms of a taxonomy for comparison"""
    if verbose:
        print("reading eval terms")

    eval_terms = []
    for eval_term in extract_eval_terms(voc):
        eval_terms.append({"original": eval_term})

    if verbose:
        print("stemming eval terms")
    porterstemmer = nltk.stem.PorterStemmer()
    for term in eval_terms:
        term["stem"] = stem(term["original"], porterstemmer)

    if verbose:
        print("tokenizing eval terms")
    for term in eval_terms:
        term["full tokens"] = my_tokenize(term["original"])

    if verbose:
        print("tokenizing eval stems")
    for term in eval_terms:
        term["stem tokens"] = my_tokenize(term["stem"])

    return eval_terms


def check_coverage_for_taxonomy(voc_id, voc, collection_terms, categories, verbose=False):
    """calculates coverage of the collection terms"""

    covered_collection_terms = {}
    counts = {}
    totals = {}
    eval_terms = prep_taxonomy(voc, verbose)
    for category in categories:
        counts[category] = 0
        totals[category] = len(collection_terms[category])
    eval_term_iter = eval_terms
    if verbose:
        eval_term_iter = tqdm(eval_terms, desc=voc_id)
    for term in eval_term_iter:
        for category in categories:
            for collection_term in collection_terms[category]:
                scores = covered(term, collection_term)
                if collection_term["original"] not in covered_collection_terms:
                    covered_collection_terms[collection_term["original"]] = [(term, scores, voc_id)]
                else:
                    covered_collection_terms[collection_term["original"]].append((term, scores, voc_id))
    for category in categories:
        for collection_term in collection_terms[category]:
            for eval_term in covered_collection_terms[collection_term["original"]]:
                if calc_total_score(eval_term[1]) > THRESHOLD:
                    counts[category] += 1
                    break

    return covered_collection_terms, {"counts": counts, "totals": totals}


def calc_total_score(scores):
    """calculates a total score from individual scores"""
    weights = (5.0, 3.0, 8.0)
    total_score = (scores[0] * weights[0] + scores[1] * weights[1] + scores[2] * weights[2]) / sum(
        weights)
    return total_score


def calc_coverage_data(covered_collection_terms):
    """calculates coverage of collection terms"""
    coverage_data = {"_matches": 0}
    for collection_term in covered_collection_terms:
        match_info = sorted(covered_collection_terms[collection_term],
                            key=lambda x: sum(x[1]),
                            reverse=True)
        total_score = calc_total_score(match_info[0][1])
        if total_score < THRESHOLD:
            continue
        coverage_data["_matches"] += 1
        coverage_data[collection_term] = []
        for eval_term, scores, _ in match_info:
            if calc_total_score(scores) < THRESHOLD:
                break
            coverage_data[collection_term].append({"term": eval_term["original"],
                                               "scores": scores,
                                               "total": total_score})
    return coverage_data


def print_summary(covered_collection_terms, categories, coverage_data, summary_data, is_final=False):
    """prints a summary for one taxonomy, how well it covers each category"""
    counts = summary_data["counts"]
    totals = summary_data["totals"]
    matches = coverage_data["_matches"]
    print("\nmatches\n-------\n")
    for covered_term, covering_terms_info in coverage_data.items():
        if covered_term == "_matches":
            continue
        print(f"{covered_term}:")
        for covering_term_info in covering_terms_info:
            print(f"\t{covering_term_info['term']}: {covering_term_info['total']} "
                  f"{covering_term_info['scores']}")
    print("\ncategory coverage scores\n"
          "------------------------\n")

    for category in categories:
        adjusted_category = (category + ':').ljust(max([len(x) + 2 for x in categories]))
        print(f"\t{adjusted_category}{counts[category] / totals[category]}")

    print("\n" + f"matches:\t\t{matches}")
    if not is_final:
        print(f"eval_terms:\t{len(list(covered_collection_terms.values())[0])}")
    print(f"collection_terms:\t{len(covered_collection_terms)}", end="\n\n")
