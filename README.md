# About

This repository contains 1) scripts used to assess the relevancy of taxonomies for research data annotation, 2) an exemplary categorized term collection used as a reference for relevancy assessment, and 3) scripts for recognizing and linking related concepts across taxonomies.

To assess a taxonomy's relevancy for annotation, we used a term collection as reference which is located in [categorized_terms.yaml](input/categorized_terms.yaml).
This term collection contains a set of terms originating from a workshop as well as from text mining research objects (specifically, posters and abstracts) related to the hybrid societies research area.
The terms of the collection were then categorized into the following six categories:
1) Research Subject
2) Research Target
3) Technology
4) Participant Demography
5) Method
6) Data Preparation

The relevance of a taxonomy is measured by calculating the coverage of each of these categories.
For this purpose, an aggregate similarity score is used, combining fuzzy string similarity, token-based similarity and path-similarity metrics.
Then, it is calculated to which extent a taxonomy covers each category using the category coverage score.
The category coverage score is calculated by counting the number of terms that are covered by the taxonomy and dividing by the number of category terms. 

This repository also contains source code used to link taxonomies, cf. the usage notes below on skos_similarity_checker and skos_linker.


## Usage

tl;dr: check your taxonomy's category term coverage by running:

    python3 category_coverage.py -e input/categorized_terms.yaml path/to/your/taxonomy.ttl

Got no taxonomy at hand? Try one of these [10.5281/zenodo.7908855](https://doi.org/10.5281/zenodo.7908855)


### category_coverage.py

This script sorts the result into the output directory. It also checks for coverage of the given expert terms and gives an overview over the current overall coverage.

    python3 category_coverage.py -e collection_terms.yaml taxonomy.ttl

Options:

- `-h` shows a help message and exits.
- `-q` reduces console output.
- `-p` shows a progress bar for the collection term coverage checks.
- `-o` specifies the output directory, where a directory for the taxonomy will be created. If other taxonomies are stored in the same output directory, they will be taken into account when showing the final overview.
- `--overwrite` specifies that any output file that already exists will be overwritten without a manual check.

The collection terms must be in a YAML file with the following format:

    <categories>:
        - <terms>

See the *categorized_terms.yaml* for an example.


### overview.py

This script prints an overview over the current collection term coverage across all taxonomies in the input directory.

    python3 overview.py -e collection_terms.yaml

Options:

- `-h` shows a help message and exits.
- `-i` specifies the input directory to use.


### skos_similarity_checker.py

This script checks for possible matches between multiple taxonomies.

    python3 skos_similarity_checker.py main_file.ttl other_file.ttl [another_file.ttl ...]

Options:

- `-h` shows a help message and exits.
- `--all` By default, only the terms of the `main_file` will be compared to the terms of the other taxonomies, but not the terms of the other taxonomies with each other. If `--all` is specified, than these other taxonomies are also compared with each other.
- `--overwrite` specifies that any output file that already exists will be overwritten without a manual check.

### skos_linker.py

Asks the user for each identified match if it is actually a match, and adds it to the result graphs if it is.

    python3 skos_linker.py main_file.ttl linked_results_main_file_other_file.json [linked_results_main_file_another_file.json ...] 

By default, only the `main_file` will be written, other SKOS files will not be modified or written. Use `--write-all` to write all graphs. The `main_file` becomes essentially meaningless if `--write-all` is used.

Options:

- `-h` shows a help message and exits.
- `-o` specifies the output directory where the taxonomy will be saved to.
- `-f` specifies the output RDF serialization format. Defaults to 'xml'. Can be any format that `rdflib` can serialize to.
- `--overwrite` specifies that any output file that already exists will be overwritten without a manual check.
- `--never-overwrite` specifies that no output file that already exists should ever be overwritten.
- `--auto-accept` specifies that all potential matches should be accepted without manual check.
- `--write-all` By default, only the `main_file` will be written, other SKOS files will not be modified or written. Use `--write-all` to write all graphs. The `main_file` becomes essentially meaningless if `--write-all` is used.


### evaluator.py and util.py

These two files contain shared functionality for the other scripts. They should not be executed directly


## License

This project is licensed under MIT license - see the [LICENSE](LICENSE) file for more information.
