"""
Adds additional links to skos files, with manual checks.
"""

import argparse
import json
import os
import sys

import rdflib
import skosify

from util import manual_label_similarity, load_graph, check_overwrite, FILE_ENDINGS, SKOS


def argparser():
    """
    Builds the argparse parser and reads the command line parameters
    """
    parser = argparse.ArgumentParser(description="Adds additional links to skos files, "
                                                 "with manual checks")
    parser.add_argument("tax_file", metavar="SKOS_FILE", type=str,
                        help="SKOS file to add links to.")
    parser.add_argument("in_files", metavar="LINK_FILE", nargs="+", type=str,
                        help="Link files from the SKOS similarity checker.")
    parser.add_argument("-o", dest="output_dir", default="linked_output/",
                        help="Output directory for the finished taxonomy. Defaults to "
                             "'linked_output/'. Will be created if it doesn't exist yet.")
    parser.add_argument("-f", dest="output_format", default="xml",
                        help="Output format. Defaults to 'xml'. Can be any format that rdflib can "
                             "serialize to.")
    parser.add_argument("--auto-accept", dest="auto_accept", action="store_true",
                        help="If this is set, then all potential matches will be added without the "
                             "manual check.")
    parser.add_argument("--overwrite", dest="overwrite_existing", action="store_true",
                        help="If this is set, then any existing output file that already exists "
                             "will be overwritten without the manual check.")
    parser.add_argument("--never-overwrite", dest="never_overwrite", action="store_true",
                        help="If this is set, then any existing output file that already exists "
                             "will never be overwritten.")
    parser.add_argument("--write-all", dest="write_all", action="store_true",
                        help="If this is set, then links will be added to all graphs, and these "
                             "additional modified graphs will also be saved.")
    return parser.parse_args()


def write_graphs(graphs, output_dir, output_format, target_taxonomy,
                 overwrite_existing=False, never_overwrite=False,):
    """
    Writes the skos graph into the output directory, checks already existing files.
    If target_taxonomy is None, then all graphs will be written instead of only the main one
    """
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    if target_taxonomy is None:
        my_iter = graphs.items()
    else:
        my_iter = ((target_taxonomy, graphs[target_taxonomy]),)
    for skos_file, graph in my_iter:
        if skos_file == "_rejects":
            continue
        file_path = os.path.join(output_dir, os.path.basename(skos_file) + \
                                 FILE_ENDINGS.get(output_format, "rdf"))

        if os.path.exists(file_path) and (never_overwrite or not check_overwrite(file_path,
                                                                                 overwrite_existing)):
            if never_overwrite:
                print(f"'{file_path}' already exists. Skipped...")
            else:
                print("Skipped...")
            continue

        voc = skosify.skosify(graph)
        with open(file_path, "w+") as out_file:
            out_file.write(voc.serialize(format=output_format).decode())


def load_graphs(potential_matches):
    """
    Loads and parses the required skos graphs for the potential matches and returns them in a
    dictionary, with the keys being the file name of the respective skos graph
    """
    graphs = {}
    for term_1, term_2 in potential_matches:
        for graph_file in (term_1["skos_file"], term_2["skos_file"]):
            if graph_file in graphs:
                continue
            try:
                graphs[graph_file] = load_graph(graph_file)
            except rdflib.exceptions.ParserError:
                print(f"Error parsing referenced skos file {graph_file} (wrong format), aborting. "
                      "use turtle or xml notation only.")
                sys.exit(1)
            except SyntaxError:
                print(f"Error parsing referenced skos file {graph_file} (syntax error), aborting")
                sys.exit(1)
    return graphs


def check_matches(potential_matches, graphs, auto_accept):
    """
    Checks all potential matches and asks the user if they are the same, unless auto_accept is True.
    If it is a real match according to the user or auto_accept, then the required triple will be
    added to each SKOS graph.
    """
    for term_1, term_2 in potential_matches:
        if auto_accept or manual_label_similarity(term_1, term_2, graphs):
            graphs[term_1["skos_file"]].add((rdflib.URIRef(term_1["concept"]),
                                             SKOS.closeMatch,
                                             rdflib.URIRef(term_2["concept"])))
            graphs[term_1["skos_file"]].add((rdflib.URIRef(term_2["concept"]),
                                             SKOS.closeMatch,
                                             rdflib.URIRef(term_1["concept"])))
            graphs[term_2["skos_file"]].add((rdflib.URIRef(term_1["concept"]),
                                             SKOS.closeMatch,
                                             rdflib.URIRef(term_2["concept"])))
            graphs[term_2["skos_file"]].add((rdflib.URIRef(term_2["concept"]),
                                             SKOS.closeMatch,
                                             rdflib.URIRef(term_1["concept"])))
            with open("storage.txt", "a+") as out_file:
                out_file.write("\n"
                               f"{term_1['skos_file']}, {term_1['concept']}, "
                               f"{term_2['skos_file']}, {term_2['concept']}")
        else:
            graphs["_rejects"].add((rdflib.URIRef(term_1["concept"]),
                                    SKOS.closeMatch,
                                    rdflib.URIRef(term_2["concept"])))
            graphs["_rejects"].add((rdflib.URIRef(term_2["concept"]),
                                    SKOS.closeMatch,
                                    rdflib.URIRef(term_1["concept"])))


def load_potential_matches(in_files):
    """
    Loads potential matches from from the given filepaths.
    """
    potential_matches = []
    for link_file_name in in_files:
        try:
            with open(link_file_name) as link_file:
                potential_matches += json.load(link_file)
        except IOError:
            print(f"Error in link file {link_file_name}, aborting")
            sys.exit(1)
    return potential_matches


def main():
    """for commandline usage, use --help for additional info."""
    args = argparser()

    if not os.path.exists(args.tax_file):
        print(f"Main taxonomy '{args.tax_file}' not found!")
        sys.exit(1)

    print("loading link files...")
    potential_matches = load_potential_matches(args.in_files)

    print("Loading required skos files...")
    graphs = load_graphs(potential_matches)
    graphs["_rejects"] = rdflib.Graph()

    print("Please check the following matches:")
    check_matches(potential_matches, graphs, args.auto_accept)

    print("Done with checks, writing output graphs...")
    if args.write_all:
        write_graphs(graphs, args.output_dir, args.output_format, None,
                     args.overwrite_existing, args.never_overwrite)
    else:
        write_graphs(graphs, args.output_dir, args.output_format, args.tax_file,
                     args.overwrite_existing, args.never_overwrite)


if __name__ == "__main__":
    main()
