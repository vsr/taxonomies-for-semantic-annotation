"""
Script that does all the things that can be done to an individual taxonomy:
- Compare terms to collection terms
- Store results in a directory structure within the output directory
"""

import os
import argparse
import json

from evaluator import check_coverage_for_taxonomy, prep, print_summary, calc_coverage_data
from util import load_graph


def argparser():
    """Builds the argparse parser and reads the command line parameters."""
    parser = argparse.ArgumentParser(description="compares taxonomy terms to collection terms")
    parser.add_argument('-q', dest='quiet', action='store_true', default=False,
                        help="reduce console output")
    parser.add_argument('-e', dest='collection_terms_filename',
                        default='input/categorized_terms.yaml',
                        help="file containing collection terms in categories")
    parser.add_argument('-p', '--progress-bar', dest='progress_bar', action='store_true',
                        help="print progress bar for each taxonomy")
    parser.add_argument("in_file", metavar="SOURCE", type=str,
                        help="Taxonomy file to read")
    parser.add_argument('-o', dest='output_dir', default='output/',
                        help="output directory for storing converted taxonomies and coverage data")
    parser.add_argument("--overwrite", dest="overwrite", action="store_true",
                        help="If this is set, then any existing output file that already exists "
                             "will be overwritten without the manual check.")
    return parser.parse_args()


def load_input_file(in_file):
    """Loads the input file."""
    voc_id = os.path.splitext(os.path.basename(in_file))[0]
    voc = load_graph(in_file)
    return voc, voc_id


def write_coverage_data(coverage_data, categories, collection_terms, voc_id, output_dir):
    """Prints final overview over all taxonomies in the output directory."""
    dump_data = {}
    for category in categories:
        dump_data[category] = {}
        for collection_term in collection_terms[category]:
            if collection_term["original"] not in coverage_data:
                continue
            dump_data[category][collection_term["original"]] = coverage_data[
                collection_term["original"]]
    dump_data["_voc_id"] = voc_id
    with open(os.path.join(output_dir, voc_id, "term_collection_coverage.json"), "w+") as out_file:
        json.dump(dump_data, out_file)


def print_overview(output_dir, categories, collection_terms):
    """Prints final overview over all taxonomies in the output directory."""
    print("final summary")
    print("=============")
    total_coverage = {}
    for directory in os.listdir(output_dir):
        if not os.path.isdir(os.path.join(output_dir, directory)):
            continue
        if not os.path.exists(os.path.join(output_dir, directory, "term_collection_coverage.json")):
            print(f"missing term_collection_coverage.json in directory {directory}")
            continue
        with open(os.path.join(output_dir, directory, "term_collection_coverage.json")) as in_file:
            voc_info = json.load(in_file)
        for category in categories:
            for collection_term in voc_info[category]:
                if collection_term not in total_coverage or \
                        total_coverage[collection_term][0]["total"] < \
                        voc_info[category][collection_term][0]["total"]:
                    total_coverage[collection_term] = [voc_info[category][collection_term][0]]
    counts = {}
    totals = {}
    total_coverage["_matches"] = 0
    final_collection_terms = []
    for category in categories:
        count = 0
        totals[category] = len(collection_terms[category])
        for collection_term in collection_terms[category]:
            final_collection_terms.append(collection_term["original"])
            if collection_term["original"] in total_coverage:
                count += 1
        counts[category] = count
        total_coverage["_matches"] += count
    final_summary_data = {"counts": counts, "totals": totals}
    print_summary(final_collection_terms, categories, total_coverage, final_summary_data, True)


def main():
    """For commandline usage only. Use --help for usage information."""

    args = argparser()

    voc, voc_id = load_input_file(args.in_file)

    if not os.path.exists(os.path.join(args.output_dir, voc_id)):
        os.makedirs(os.path.join(args.output_dir, voc_id))

    categories, collection_terms, _ = prep(args.collection_terms_filename, not args.quiet)

    covered_collection_terms, summary_data = check_coverage_for_taxonomy(voc_id,
                                                                         voc,
                                                                         collection_terms,
                                                                         categories,
                                                                         not args.quiet)

    coverage_data = calc_coverage_data(covered_collection_terms)

    write_coverage_data(coverage_data, categories, collection_terms, voc_id, args.output_dir)

    if not args.quiet:
        print_summary(covered_collection_terms, categories, coverage_data, summary_data)
        print_overview(args.output_dir, categories, collection_terms)


if __name__ == "__main__":
    main()
