"""contains utility function for evaluator and skos similarity checker / linker"""

import re

import rdflib
from nltk.corpus import wordnet as wn
from fuzzywuzzy import fuzz


# Constants:
SKOS = rdflib.namespace.SKOS
DCT = rdflib.namespace.DCTERMS
RDF = rdflib.namespace.RDF

FILE_ENDINGS = {
    "xml": ".rdf",
    "turtle": ".ttl",
    "ttl": ".ttl",
    "turtle2": ".ttl",
    "json-ld": ".json",
    "ntriples": ".nt",
    "nt": ".nt",
    "nt11": ".nt",
    "trig": ".trig",
    "trix": ".trix",
    "nquads": ".nquads"
}


def check_overwrite(filepath, overwrite=False):
    """
    asks the user if a given file should be overwritten. Always True if overwrite is set to True.
    """
    if overwrite:
        return True
    inp = ""
    while inp not in ("y", "n"):
        inp = input(f"'{filepath}' already exists. Overwrite? (y/n) ")
    if inp == "y":
        return True
    return False


def load_graph(filepath):
    """loads an rdf graph from a given filepath"""
    graph = rdflib.Graph()
    if filepath.endswith(".ttl"):
        graph.parse(filepath, format="turtle")
    elif filepath.endswith(".rdf"):
        graph.parse(filepath, format="xml")
    else:
        graph.parse(filepath)
    return graph


def stem(term, stemmer):
    """
    Stems a given term using the given stemmer.
    If the term is made up from multiple words or parts, it gets split and each part stemmed
    individually.
    """
    return " ".join([stemmer.stem(word) for word in re.split(r"[ \-\/()]", term)])


def my_tokenize(in_str):
    """
    Tokenizes a string into multiple words
    """
    term_set = set(in_str.split(" "))
    while "" in term_set:
        term_set.remove("")
    return term_set


def str_token_set_ratio(str_a, str_b, full_set_overlap=False):
    """
    Calculates a similarity ration of two strings based on the tokens the strings are made up from.
    Essentially it calculates the overlap of the two terms, but ignoring the order.
    """
    set_a = my_tokenize(str_a)
    set_b = my_tokenize(str_b)
    if min(len(set_a), len(set_b)) == 0:
        return 0.0
    matches = 0
    for token in set_a:
        if token in set_b:
            matches += 1
    if full_set_overlap:
        return matches/(len(set_a) + len(set_b) - matches)
    return matches/(2*min(len(set_a), len(set_b))-matches)


def wn_semantic_similarity_pairwise(set_a, set_b, similarity_func=wn.path_similarity):
    """
    Calculates the similarity between two sets of tokens, using the supplied similarity function on
    synsets for the tokens.
    """
    wn_set_a = []
    for token in set_a:
        synsets = wn.synsets(token)
        if not synsets:
            continue
        wn_set_a.append(synsets[0])
    wn_set_b = []
    for token in set_b:
        synsets = wn.synsets(token)
        if not synsets:
            continue
        wn_set_b.append(synsets[0])
    total_similarity = 0.0
    if not wn_set_a or not wn_set_b:
        return 0.0
    short_set = wn_set_a
    long_set = wn_set_b
    if len(short_set) > len(long_set):
        short_set, long_set = long_set, short_set
    for token_a in short_set:
        similarity = 0.0
        for token_b in long_set:
            new_similarity = similarity_func(token_a, token_b)
            if new_similarity is None:
                new_similarity = 0.0
            if new_similarity > similarity:
                similarity = new_similarity
        total_similarity += similarity
    return total_similarity / len(short_set)


def covered(eval_term, collection_term, full_set_overlap=False):
    """returns true if the term covers the collection_term, otherwise false"""
    scores = [
        (fuzz.token_set_ratio(eval_term["stem"], collection_term["stem"]) / 100.0, 3.0),
        (str_token_set_ratio(eval_term["stem"], collection_term["stem"], full_set_overlap), 5.0),
        (wn_semantic_similarity_pairwise(eval_term["full tokens"],
                                         collection_term["full tokens"],
                                         wn.path_similarity), 8.0)
    ]
    return (scores[0][0], scores[1][0], scores[2][0])


def auto_label_similarity(term_1, term_2):
    """calculates a total similarity for two preprocessed terms and decides if they are the same."""
    weights = (5.0, 3.0, 8.0)
    scores = covered(term_1, term_2, True)
    total_score = sum([x*w for x, w in zip(scores, weights)])/sum(weights)
    if total_score < 1.0:
        return False
    return True


def manual_label_similarity(term_1, term_2, graphs):
    """
    asks the user if two terms are the same. The label and definitions are taken from the graphs
    """
    # If we already have matched/rejected these terms, then don't ask again
    t_1 = rdflib.URIRef(term_1['concept'])
    t_2 = rdflib.URIRef(term_2['concept'])
    if (t_1, SKOS.closeMatch, t_2) in graphs[term_1["skos_file"]] or \
       (t_1, SKOS.closeMatch, t_2) in graphs[term_2["skos_file"]] or \
       (t_2, SKOS.closeMatch, t_1) in graphs[term_1["skos_file"]] or \
       (t_2, SKOS.closeMatch, t_1) in graphs[term_2["skos_file"]]:
        print("auto add")
        return True
    if (t_1, SKOS.closeMatch, t_2) in graphs["_rejects"] or \
       (t_2, SKOS.closeMatch, t_1) in graphs["_rejects"]:
        print("auto reject")
        return False

    print("\n\n")
    for ind, (graph_name, concept) in enumerate(((term_1["skos_file"], term_1["concept"]),
                                                 (term_2["skos_file"], term_2["concept"]))):
        concept = rdflib.URIRef(concept)
        graph = graphs[graph_name]
        print(f"({ind+1}) '{', '.join(graph.objects(concept, SKOS.prefLabel))}' ({concept})")
        for extra in ("altLabel", "hiddenLabel", "definition", "note", "scopeNote", "historyNote",
                      "editorialNote", "changeNote", "example", "notation"):
            extra_info = "\n".join(list(graph.objects(concept, SKOS[extra])))
            if extra_info:
                print(f"{extra}: {extra_info}")
        if not ind:
            print("\nvs.\n")
    inp = ""
    while inp not in ("y", "n"):
        inp = input("Same? (y/n): ")
    return bool(inp == "y")
